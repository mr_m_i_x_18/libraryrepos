﻿using Library.DataBase.Entities;
using Library.ViewModels.Books;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Library.Domain.Interfaces;
using AutoMapper;
using System.Collections.Generic;

namespace Library.Controllers
{
    public class BooksController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Book> _repository;

        public BooksController(IRepository<Book> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [Route("Books/List/{genre}")]
        public async Task<IActionResult> List(int genre)
        {
            var books = await _repository.GetAll()
                .OrderBy(x => x.Id).Where(c => c.GenreId == genre).ToListAsync();
            var model = _mapper.Map<List<AllBookListViewModel>>(books);

            return View(model);
        }

        public async Task<IActionResult> Index(string searchString, string category)
        {
            var books = category switch
            {
                "name" => _repository.GetAll().Where(c => c.Name.Contains(searchString)),
                "author" => _repository.GetAll().Where(c => c.Author.Contains(searchString)),
                "publisher" => _repository.GetAll().Where(c => c.Publisher.Contains(searchString)),
                _ => _repository.GetAll()
            };
            var book = await books.OrderBy(x => x.Id).ToListAsync();
            var model = _mapper.Map<List<AllBookListViewModel>>(books);
            return View(model);
        }

        public async Task<IActionResult> CurrentBook(int bookId)
        {
            var book = await _repository.GetByIdAsync(bookId);
            var model = _mapper.Map<AllBookListViewModel>(book);
            return View(model);
        }
    }
}


