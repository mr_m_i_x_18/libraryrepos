﻿using Library.Domain.Interfaces;
using Library.DataBase.Entities;
using Library.ViewModels;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;
using System.Linq;
using Library.ViewModels.Books;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Library.Controllers
{
    [Authorize(Roles = "librarian")]
    public class LibrarianController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILibrarianRepository _repositoryLib;
        private readonly IRepository<Book> _repositoryBook;
        private readonly IRepository<Genre> _repositoryGenre;
        private readonly IRepository<Order> _repositoryOrder;
        private readonly IOrderRepository _repositoryOrd;

        public LibrarianController(ILibrarianRepository repositoryLib, IOrderRepository repositoryOrd, IRepository<Book> repositoryBook, IRepository<Order> repositoryOrder, IRepository<Genre> repositoryGenre, IMapper mapper)
        {
            _repositoryLib = repositoryLib;
            _repositoryOrd = repositoryOrd;
            _repositoryBook = repositoryBook;
            _repositoryOrder = repositoryOrder;
            _repositoryGenre = repositoryGenre;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var books = await _repositoryBook.GetAll().OrderBy(c => c.Id).ToListAsync();
            var model = _mapper.Map<List<AllBookListViewModel>>(books);
            return View(model);
        }

        public async Task<IActionResult> AddBook()
        {
            var genres = await _repositoryGenre.GetAll().ToListAsync();
            ViewBag.Genres = new SelectList(genres, "Id", "GenreName");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddBook(AddBookViewModel model)
        {
            if (ModelState.IsValid)
            {
                var book = _mapper.Map<Book>(model);
                await _repositoryBook.AddAsync(book);
            }

            return RedirectToAction("Index", "Librarian");
        }

        public async Task<ActionResult> DeleteBook(int bookId)
        {
            var book = await _repositoryBook.GetByIdAsync(bookId);
            await _repositoryBook.RemoveAsync(book);
            return RedirectToAction("Index", "Librarian");
        }

        public async Task<IActionResult> EditBook(int bookId)
        {
            var book = await _repositoryBook.GetByIdAsync(bookId);
            var model = _mapper.Map<EditBookViewModel>(book);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditBook(EditBookViewModel model)
        {
            var book = await _repositoryBook.GetByIdAsync(model.Id);
            book.Name = model.Name;
            book.Desc = model.Desc;
            book.Img = model.Img;
            book.Publisher = model.Publisher;
            await _repositoryBook.UpdateAsync(book);


            return RedirectToAction("Index", "Librarian");
        }

        public async Task<IActionResult> AllOrders()
        {
            var split = await _repositoryLib.GetAllOrdersAsync();
            return View(split);
        }

        public async Task<ActionResult> GiveBook(int bookId)
        {
            await _repositoryLib.GiveBookAsync(bookId);
            return RedirectToAction("AllOrders", "Librarian");
        }

        public async Task<ActionResult> TakeBackBook(int bookId)
        {
            await _repositoryLib.TakeBookAsync(bookId);
            return RedirectToAction("AllOrders", "Librarian");
        }

        public async Task<ActionResult> DeleteOrder(int bookId, int orderId)
        {
            var book = await _repositoryBook.GetByIdAsync(bookId);
            book.Availabile = true;
            book.IsGiven = false;
            await _repositoryBook.UpdateAsync(book);
            var order = await _repositoryOrder.GetByIdAsync(orderId);
            await _repositoryOrder.RemoveAsync(order);
            return RedirectToAction("AllOrders", "Librarian");
        }
    }
}
