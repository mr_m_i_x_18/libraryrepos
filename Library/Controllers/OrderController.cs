﻿using Library.Domain.Interfaces;
using Library.DataBase.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Library.Controllers
{
    public class OrderController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IOrderRepository _orderRepository;
        private readonly IRepository<Order> _repositoryOrder;
        private readonly IRepository<Book> _repositoryBook;
        public OrderController(IOrderRepository repository, UserManager<User> userManager, IRepository<Order> repositoryOrder, IRepository<Book> repositoryBook)
        {
            _orderRepository = repository;
            _userManager = userManager;
            _repositoryOrder = repositoryOrder;
            _repositoryBook = repositoryBook;
        }

        public async Task<ActionResult> Ordered(int bookId)
        {
            IdentityUser user = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            var item = new Order()
            {
                ClientName = user.UserName,
                Phone = user.PhoneNumber,
                Email = user.Email,
                BookId = bookId,
                OrderTime = DateTime.Now
            };
            await _repositoryOrder.AddAsync(item);
            var book = await _repositoryBook.GetByIdAsync(bookId);
            book.Availabile = false;
            await _repositoryBook.UpdateAsync(book);

            return RedirectToAction("Index", "Books");
        }

        public async Task<IActionResult> MyOrders()
        {
            IdentityUser userName = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            IEnumerable<Split> split = await _orderRepository.GetOrderMyNameAsync(userName.UserName);

            return View(split);
        }

        public async Task<ActionResult> DeleleteOrder(int orderId, int bookId)
        {
            var book = await _repositoryBook.GetByIdAsync(bookId);
            book.Availabile = true;
            book.IsGiven = false;
            await _repositoryBook.UpdateAsync(book);
            var order =  await _repositoryOrder.GetByIdAsync(orderId);
            await _repositoryOrder.RemoveAsync(order);

            return RedirectToAction("MyOrders", "Order");
        }

    }
}
