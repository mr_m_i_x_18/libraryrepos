﻿using FluentValidation;
using Library.ViewModels;

namespace Library.FluentValidation
{
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        public LoginViewModelValidator()
        {
            RuleFor(x => x.Email).EmailAddress().WithMessage("Введите корректный электронныый адрес");
        }
    }
}
