﻿using FluentValidation;
using Library.ViewModels.Users;
using System;

namespace Library.FluentValidation
{
    public class CreateUserViewModelValidator : AbstractValidator<CreateUserViewModel>
    {
        public CreateUserViewModelValidator()
        {
            RuleFor(x => x.PhoneNumber).NotNull().Matches(@"^\d{10}$").WithMessage("Веедите 10 значный номер телефона без '+7'");
            RuleFor(x => x.Email).EmailAddress().Length(3, 100).WithMessage("Введите корректный электронный адрес");
            RuleFor(x => DateTime.Now.Year - x.Year).GreaterThanOrEqualTo(16).WithMessage("Регистрация возможно только для пользователей старше 16 лет");
        }
    }
}
