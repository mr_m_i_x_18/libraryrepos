﻿using FluentValidation;
using Library.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.FluentValidation
{
    public class EditBookViewModelValidator : AbstractValidator<EditBookViewModel>
    {
        public EditBookViewModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Desc).NotEmpty().MinimumLength(30).WithMessage("Введите описание больше 30 символов");
            RuleFor(x => x.Publisher).NotEmpty();
        }
    }
}
