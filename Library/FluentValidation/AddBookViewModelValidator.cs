﻿using FluentValidation;
using Library.ViewModels;

namespace Library.FluentValidation
{
    public class AddBookViewModelValidator : AbstractValidator<AddBookViewModel>
    {
        public AddBookViewModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Desc).NotEmpty().MinimumLength(30).WithMessage("Введите описание больше 30 символов");
            RuleFor(x => x.Author).NotEmpty();
            RuleFor(x => x.Publisher).NotEmpty();
        }
    }
}
