﻿using Library.DataBase.Entities;
using Library.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Quartz;
using System;
using System.Threading.Tasks;

namespace Library.Job
{
    public class TimeChecker :IJob
    {
        private readonly IRepository<Book> _repositoryBook;
        private readonly IRepository<Order> _repositoryOrder;

        public TimeChecker( IRepository<Book> repositoryBook, IRepository<Order> repositoryOrder)
        {
            _repositoryBook = repositoryBook;
            _repositoryOrder = repositoryOrder;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var books = await _repositoryBook.GetAll().ToListAsync();
            var orders = await _repositoryOrder.GetAll().ToListAsync();
            var time =  new TimeSpan(24, 0, 0);
            foreach(var order in  orders)
            {
                var book = books.Find(a => a.Id == order.BookId);
                if(DateTime.Now.Subtract(order.OrderTime) > time && !book.IsGiven && !book.Availabile)
                {
                    book.Availabile = true;
                    book.IsGiven = true;
                    await _repositoryBook.UpdateAsync(book);
                }
            }
        }
    }
}
