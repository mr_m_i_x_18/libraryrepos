﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels.Order
{
    public class OrdersListViewModel
    {
        int Id { get; set; }
        string ClientName { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string BookName { get; set; }
        string BookAuthor { get; set; }
        string BookPublisher { get; set; }
        bool BookAvailabile { get; set; }
        bool BookIsGiven { get; set; }
        DateTime OrderTime { get; set; }
    }
}
