﻿using System.ComponentModel.DataAnnotations;

namespace Library.ViewModels
{
    public class AddBookViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Desc { get; set; }

        public string Author { get; set; }

        public string Img { get; set; }

        public string Publisher { get; set; }

        public bool Availabile { get; set; } = true;

        public bool IsGiven { get; set; } = false;

        public int GenreId { get; set; }
    }
}
