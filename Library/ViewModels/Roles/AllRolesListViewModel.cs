﻿namespace Library.ViewModels.Roles
{
    public class AllRolesListViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
