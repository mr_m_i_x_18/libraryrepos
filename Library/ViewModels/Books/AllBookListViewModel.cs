﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels.Books
{
    public class AllBookListViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Desc { get; set; }

        public string Author { get; set; }

        public string Img { get; set; }

        public string Publisher { get; set; }
        public bool Availabile { get; set; }
    }
}
