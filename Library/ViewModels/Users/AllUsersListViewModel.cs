﻿namespace Library.ViewModels.Users
{
    public class AllUsersListViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public int Year { get; set; }
    }
}
