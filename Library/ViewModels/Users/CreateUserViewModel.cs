﻿namespace Library.ViewModels.Users
{
    public class CreateUserViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public int Year { get; set; }
    }
}
