﻿using AutoMapper;
using FluentValidation;
using Library.DataBase;
using Library.DataBase.Entities;
using Library.Domain.Interfaces;
using Library.FluentValidation;
using Library.Infrastructure.Repository;
using Library.Job;
using Library.Mappings;
using Library.ViewModels;
using Library.ViewModels.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace Library.Extensions
{
    public static class StartUpExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<ILibrarianRepository, LibrarianRepository>();
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IValidator<RegisterViewModel>, RegisterViewModelValidator>();
            services.AddTransient<IValidator<CreateUserViewModel>, CreateUserViewModelValidator>();
            services.AddTransient<IValidator<AddBookViewModel>, AddBookViewModelValidator>();
            services.AddTransient<IValidator<EditBookViewModel>, EditBookViewModelValidator>();
            services.AddTransient<IValidator<EditUserViewModel>, EditUserViewModelValidator>();
            services.AddTransient<IValidator<LoginViewModel>, LoginViewModelValidator>();
            services.AddIdentity<User, IdentityRole>(opts =>
            {
                opts.Password.RequiredLength = 5;   // минимальная длина
                opts.Password.RequireNonAlphanumeric = false;   // требуются ли не алфавитно-цифровые символы
                opts.Password.RequireLowercase = true; // требуются ли символы в нижнем регистре
                opts.Password.RequireUppercase = false; // требуются ли символы в верхнем регистре
                opts.Password.RequireDigit = true; // требуются ли цифры
            })
 .AddEntityFrameworkStores<AppDBContent>();
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionScopedJobFactory();

                q.AddJob<TimeChecker>(options =>
                {
                    options.WithIdentity("trigger1", "group1")
                        .Build();
                });

                q.AddTrigger(options =>
                {
                    options.ForJob("trigger1", "group1")
                        .StartNow()
                        .WithSimpleSchedule(x =>
                            x.WithIntervalInSeconds(2)
                                .RepeatForever());
                });
            });

            services.AddQuartzServer(options =>
            {
                options.WaitForJobsToComplete = true;
            });

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
