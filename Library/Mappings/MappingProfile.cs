﻿using AutoMapper;
using Library.DataBase.Entities;
using Library.ViewModels;
using Library.ViewModels.Books;
using Library.ViewModels.Roles;
using Library.ViewModels.Users;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Book, AllBookListViewModel>();
            CreateMap<AddBookViewModel, Book>();
            CreateMap<Book, EditBookViewModel>();
            CreateMap<IdentityRole, AllRolesListViewModel>();
            CreateMap<User, AllUsersListViewModel>();
        }
    }
}
