﻿using Library.Domain.Interfaces;
using Library.DataBase.Entities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Library.DataBase;

namespace Library.Infrastructure.Repository
{
    public class LibrarianRepository : ILibrarianRepository
    {
        private readonly AppDBContent appDBContent;
        public LibrarianRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public async Task GiveBookAsync(int bookId)
        {
            var book = await appDBContent.Book.FindAsync(bookId);
            book.IsGiven = true;
            await appDBContent.SaveChangesAsync();
        }

        public async Task TakeBookAsync(int bookId)
        {
            Book book = await appDBContent.Book.FindAsync(bookId);
            book.IsGiven = false;
            book.Availabile = true;
            await appDBContent.SaveChangesAsync();
        }

        public async Task<IEnumerable<Split>> GetAllOrdersAsync()
        {
            var order = await appDBContent.Order.ToListAsync();
            var book = await appDBContent.Book.ToListAsync();
            var split = order
                .Join(book,
                c => c.BookId,
                t => t.Id,
                (c, t) => new Split { Order = c, Book = t }
                );
            return split;
        }
    }
}
