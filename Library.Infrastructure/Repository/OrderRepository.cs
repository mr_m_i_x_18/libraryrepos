﻿using Library.Domain.Interfaces;
using Library.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Library.DataBase;

namespace Library.Infrastructure.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly AppDBContent appDBContent;

        public OrderRepository(AppDBContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }
        public async Task<IEnumerable<Split>> GetOrderMyNameAsync(string name)
        {
            var order = await appDBContent.Order.Where(c => c.ClientName == name).ToListAsync();
            var book = appDBContent.Book.ToList();
            var split = order
                .Join(book,
                c => c.BookId,
                t => t.Id,
                (c, t) => new Split { Order = c, Book = t }
                );
            return split;
        }

    }
}
