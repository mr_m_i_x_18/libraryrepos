﻿using Library.Domain.Interfaces;
using Library.DataBase;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Infrastructure.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly AppDBContent context;
        private readonly DbSet<TEntity> dbSet;
        public Repository(AppDBContent context)
        {
            this.context = context;
            dbSet = context.Set<TEntity>();
        }

        public async Task AddAsync(TEntity item)
        {
            dbSet.Add(item);
            await context.SaveChangesAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return dbSet;
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await dbSet.FindAsync(id);
        }

        public async Task RemoveAsync(TEntity item)
        {
            dbSet.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TEntity item)
        {
            dbSet.Update(item);
            await context.SaveChangesAsync();
        }
    }
}
