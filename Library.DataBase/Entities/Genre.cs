﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.DataBase.Entities
{
    public class Genre
    {
        public int Id { get; set; }

        public string GenreName { get; set; }

        public List<Book> BookList { get; set; }
    }
}
