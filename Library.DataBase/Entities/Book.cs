﻿namespace Library.DataBase.Entities
{
    public class Book
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Desc { get; set; }

        public string Author { get; set; }

        public string Img { get; set; }

        public string Publisher { get; set; }

        public bool Availabile { get; set; }

        public bool IsGiven { get; set; }

        public int GenreId { get; set; }

        public virtual Genre Genre { get; set; }
    }
}
