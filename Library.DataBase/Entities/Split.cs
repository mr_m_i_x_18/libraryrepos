﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.DataBase.Entities
{
    public class Split
    {
        public Order Order { get; set; }
        public Book Book { get; set; }
    }
}
