﻿using Library.DataBase.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Domain.Interfaces
{
    public interface ILibrarianRepository
    {
        Task GiveBookAsync(int bookId);
        Task TakeBookAsync(int bookId);
        public Task<IEnumerable<Split>> GetAllOrdersAsync();
    }
}
