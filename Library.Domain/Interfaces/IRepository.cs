﻿using System.Linq;
using System.Threading.Tasks;

namespace Library.Domain.Interfaces
{
    public interface IRepository<TEntity>
    {
        public IQueryable<TEntity> GetAll();
        public Task<TEntity> GetByIdAsync(int id);
        public Task RemoveAsync(TEntity item);
        public Task AddAsync(TEntity item);
        public Task UpdateAsync(TEntity item);
    }
}
