﻿using Library.DataBase.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Domain.Interfaces
{
    public interface IOrderRepository
    {
        Task <IEnumerable<Split>> GetOrderMyNameAsync(string name);
    }
}
